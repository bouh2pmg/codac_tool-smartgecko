# Import flask dependencies
from flask import (
    Blueprint, request, render_template,
    flash, redirect
)

# Import module forms
from form import LoginForm

# Import module models (i.e. User)
from models import User

from flask_admin.base import AdminIndexView, expose
from flask_admin.contrib.mongoengine import ModelView

from flask_login import (
    current_user, login_required,
    login_user, logout_user
)
from app import login_manager

@login_manager.user_loader
def load_user(user_id):
    if user_id:
        return User.objects.get(id=user_id)

mod_adm = Blueprint('adm', __name__, url_prefix='/admin')

@mod_adm.route('/login/', methods=['GET', 'POST'])
def login():
    next = "/admin"
    form = LoginForm(request.form)

    if form.validate():
        if len(User.objects) == 0:
            user = User(email=form.email.data)
            user.set_password(form.password.data)
            user.save()
        else:
            try:
                user = User.objects.get(
                    email=form.email.data,
                )
            except DoesNotExist:
                user = None
                form['email'].errors.append('Email not found')
                form['password'].errors.append('Password incorrect')

            if user is not None:
                if user.check_password(form.password.data):
                    login_user(user)

                return redirect(next)

            else:
                flash('Wrong email or password', 'error-message')

    else:
        next = request.args.get('next')

    return render_template("adm/login.html", form=form, next=next)