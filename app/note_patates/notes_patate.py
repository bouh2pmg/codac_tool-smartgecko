#! /usr/bin/env python3

import requests
from requests.auth import HTTPBasicAuth


def generate(argv):

    # vars
    headers = {"Content-Type": "multipart/form-data"}
    auth = HTTPBasicAuth('adminwac', 'FkNVJhnFqU')
    form = {"module": "", "project_slug": "", "instance": "", "run_type": "git", "tenant": ""}
    url_csv = "http://patate.io:4222/csv"
    url = "http://patate.io/"
    res = "";

    # prepare
    try:
        form["module"] = argv[0]
        form["project_slug"] = argv[1]
        form["instance"] = argv[2]
        form["tenant"] = "@coding-academy.fr"
        files = {"graduation": open(argv[3], "rb")}
    except IndexError:
        raise SystemExit("error at infos: {0}".format(usage))

    print(form)
    # execute
    try:
        req = requests.Request("POST", url_csv.strip(), data=form, auth=auth, files=files)
        prep = req.prepare()
        s = requests.Session()
        res = s.send(prep)
    except requests.RequestException as e:
        raise SystemExit(e)
    print (res.text)

    # get grades
    filename = "{0}_{1}_{2}.csv".format(argv[0], argv[1], argv[2])
    url = url + filename
    print(url)
    if res.status_code == 201:
        try:
            res = requests.get(url, stream=True)
            with open("./grades/{0}".format(filename), "w+b") as f:
                for chunk in res.iter_content(chunk_size=1024):
                    f.write(chunk)
        except OSError as e:
            raise SystemExit(e)
        except requests.RequestException as e:
            print("fail")
            raise SystemExit(e)
    return 0

if __name__ == '__main__':
    import sys
    baremes = []
    slugs = []

    if len(sys.argv) < 5:
        print("./notes_patates.py slug_file bareme_file module_code instance_code domain")
        exit(1)

    try:
        with open(sys.argv[2]) as slug_file:
            for line in slug_file:
                slugs += [line.strip()]
        with open(sys.argv[1]) as bar_file:
            for line in bar_file:
                baremes += [line.strip()]
        end = len(slugs)
        for i in range(0, end):
            print([sys.argv[3]] + [slugs[i]] + sys.argv[4:] + [baremes[i]])
            generate([sys.argv[3]] + [slugs[i]] + sys.argv[4:] + [baremes[i]])
    except IOError as e:
        raise SystemExit(e)
