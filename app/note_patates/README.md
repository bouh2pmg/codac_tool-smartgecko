# Note Patate #

note patate is a script to generate grading CSV on the patate.io:4222/csv route it is used this way:

```bash
$> ./notes\_patate.py grading\_schemes.txt slug\_list.txt module\_code instance\_code @domain
```
example:

```bash
$> ./notes\_patate.py grading\_schemes.txt slug\_list.txt C-CPE-042 PAR-1-3 @coding-academy.fr
```

the script will ask the service to generate the grading file and download it. It should be directly importable in the intranet.

the bareme and slug files will one path to a bareme file or one slug per line.
example:

| baremes.txt | slugs.txt |
|----|---|
|./bar\_c/bareme\_d02.txt | C1CPE042p2 |
|./bar\_c/bareme\_d03.txt | C1CPE042p3 |
|./bar\_c/bareme\_d04.txt | C1CPE042p4 |
|./bar\_c/bareme\_d05.txt | C1CPE042p5 |
|./bar\_c/bareme\_d06.txt | C1CPE042p6 |
|./bar\_c/bareme\_d07.txt | C1CPE042p7 |
|./bar\_c/bareme\_d08.txt | C1CPE042p8 |
|./bar\_c/bareme\_d12.txt | C1CPE042p12 |

the slugs and bareme MUST match.

## Todo ##

* Add php slugs and grading\_schemes
