# forms.py
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import Required, Email

class LoginForm(FlaskForm):
	email = StringField('E-mail', validators=[Email(), Required(
		message='Forgot your email address?')], default="me@epitech.eu",
	)
	password = PasswordField('', validators=[Required(
		message='Must provide a password. ;-)')], default="fra", render_kw={"placeholder": "Password"}
	)
	remember = BooleanField('Remember me')