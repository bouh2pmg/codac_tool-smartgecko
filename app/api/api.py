import requests
import json
import os

from . import data
from app import config

STUDENTS = os.path.join(config.BASE_DIR, 'students.txt')

def get_students():
	return open(STUDENTS).read().split()

def get_city():
	return data.CITY

def get_year():
	return data.YEAR

class Api:
	def __init__(self, year, city, slug, module):
		self.year = year
		self.city = city
		self.slug = slug
		self.module = module

	def get_trace(self, user):
		url = data.BASE_URL+"job/"+self.module+"/job/"+self.slug+"/job/"+self.year+"/job/"+self.city+"/job/"+user+"/lastSuccessfulBuild/artifact/trace.txt"
		r = requests.post(url, auth=(data.USER_ID, data.TOKEN_ID))
		if r.status_code != 200:
			raise ValueError(r.reason)
		return r.text
		
	
	def manage_trace(self, trace, bar):
		resp = "".join(trace.split()).split("Exercise")[1:]
		for idx, val in enumerate(resp):
			resp[idx] = resp[idx][:5]
		note = 0
		note = str(sum([int(bar[idx]) for idx, val in enumerate(resp) if "OK" in val]))
		return {"note": note, "commentaire": "/".join(resp)}