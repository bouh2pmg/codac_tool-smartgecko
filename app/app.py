from flask import Flask, render_template, request, flash, abort, redirect, url_for, send_file
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

from .data.barem import Barem, Slug
from .api.api import Api, get_students, get_city, get_year

try:
	from StringIO import StringIO
except ImportError:
	from io import StringIO, BytesIO
from . import config
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URL
app.config['SECRET_KEY'] = "oulalacLeSecR3t"
db = SQLAlchemy(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(50), unique=True)
	password = db.Column(db.String(80))

	def __str__(self):
		return self.username

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

@app.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		user = User.query.filter_by(username=request.form['email']).first()
		if user:
			if check_password_hash(user.password, request.form['password']):
				login_user(user, True)
				return redirect('/' if not request.args.get('next') else request.args.get('next'))
			
	return render_template('login.html')

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect('/login')

@app.route('/', methods=['GET', 'POST'])
@login_required
def index():
	if request.method == 'POST':
		barem = request.form['barem'].replace("\r\n", "")
		slug = request.form['slug'].replace("\r\n", "")
		api = Api(get_year(), get_city(), slug, "C-CPE-042" if slug.find("CPE") != -1 else "C-COD-150")
		notes = {}
		for student in get_students():
			try:
				trace = api.get_trace(student)
			except ValueError:
				print("Can't access to job for " + student)
			notes[student] = api.manage_trace(trace, barem.split(';'))
		s = '\n'.join([';'.join([k, v["note"], v["commentaire"]]) for k, v in notes.items()])
		return send_file(BytesIO(s.encode()), attachment_filename='result.csv', as_attachment=True)
	return render_template('index.html', barems=Barem.parse(), slugs=Slug.parse())