import os

from app import config

BASE_DIR = os.path.join(config.BASE_DIR, 'note_patates')

BAREM_C = os.path.join(BASE_DIR, 'bar_c')
BAREM_PHP = os.path.join(BASE_DIR, 'bar_php')

SLUG_C = os.path.join(BASE_DIR, "c_slugs/c_slugs.txt")
SLUG_PHP = os.path.join(BASE_DIR, "php_slugs/php_slugs.txt")

class Barem:
	def __init__(self):
		self.barem = self.parse()

	@staticmethod
	def	parse():
		return {
			"c": [{"day": int(f.replace(".txt", "").replace("bareme_d", "")), "barem": open(os.path.join(BAREM_C, f)).read()} for f in os.listdir(BAREM_C) if os.path.isfile(os.path.join(BAREM_C, f)) and not f.startswith(".")],
			"php": [{"day": int(f.replace(".txt", "").replace("bareme_php", "")), "barem": open(os.path.join(BAREM_PHP, f)).read()} for f in os.listdir(BAREM_PHP) if os.path.isfile(os.path.join(BAREM_PHP, f)) and not f.startswith(".")],
		}
	
	def	getAll(self):
		return self.barem

	def	getBaremFromDay(self, pool, day):
		for d in self.barem[pool]:
			if d["day"] == day:
				return d["barem"]

class Slug:
	def __init__(self):
		self.slug = self.parse()
	
	@staticmethod
	def parse():
		return {
			"c": open(SLUG_C).read().split(),
			"php": open(SLUG_PHP).read().split(),
		}
	def getAll(self):
		return self.slug
	
	def getC(self):
		return self.slug["c"]
	def getPHP(self):
		return self.slug["php"]