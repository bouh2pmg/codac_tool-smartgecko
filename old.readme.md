# SmartGecko

## Utilité
Le but de ce projet, est de faciliter l’interaction que nous avons avec Jenkins dans le but de ramasser des notes (ou peut-être lancer des moulinettes ?)

## Fonctionnement
Un outil a été mis en place (accessible aux pédagos/assistants) afin de récupérer des notes en quelques clics. Pour ce faire, nous utiliserons :

 - Python Flask pour le back et le stockage
 - Sub-modules :
		 - [Notes Patates](https://gitlab.com/geckos/note_patates/), pour récupérer les barèmes

## Utilisation
Le site est hébergé [ici](https://smartgecko.herokuapp.com/).
Pour vous connecter, veuillez saisir les identifiants définit par l'administrateur.

## Tâches à répéter lors d'une nouvelle promotion
Il faut :

 - Update le fichier students.txt, dans [app](https://gitlab.com/geckos/smartgecko/blob/master/app/students.txt)
- En cas de changements dans [Notes patates](https://gitlab.com/geckos/smartgecko/tree/master/app/note_patates), il faudra update les submodules.